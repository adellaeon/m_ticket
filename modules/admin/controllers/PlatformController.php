<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Platform;
use app\models\PlatformSerch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ImageUpload;
use yii\web\UploadedFile;

use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * PlatformController implements the CRUD actions for Platform model.
 */
class PlatformController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Platform models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlatformSerch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Platform model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Platform model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Platform();

        if ( $model->load(Yii::$app->request->post()))
        {
            //если этот номер сортировки уже занят, то перемещаем все остальные поля на +1
            Platform::updateSort($model->sort);
            $model->short_description = mb_strimwidth($model->description, 0, 255, "...");
            if($model->save())
            {
                $image = new ImageUpload;
                $platform = $this->findModel($model->id);
                $file = UploadedFile::getInstance($model, 'img');

                if($file){
                    $image_save = $image->uploadFile($file, $platform->img, $this->getFolder());
                    $dir = $this->getFolder();
                    $name = str_replace($dir, '', $image_save);
                    $name = str_replace('/', '', $name);
                    $platform->saveImage($name);

                    //crop img
                   /* $photo = Image::getImagine()->open($dir . $name);
                    $photo->thumbnail(new Box(500, 325))->save($dir . 'thumbs/500x325_'. $name, ['quality' => 90]);
                    $photo->thumbnail(new Box(800, 500))->save($dir . 'thumbs/800x500_'. $name, ['quality' => 90]);*/
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        $sort = $this->getListSortNew();
        $tmp = $sort;
        $model->sort = array_pop($tmp);
        $model->status = Platform::STATUS_ACTIVE;

        return $this->render('create', [
            'model' => $model,
            'sort' => $sort,
            'status' => $this->getListStatus()
        ]);
    }

    /**
     * Updates an existing Platform model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $sort = $model->sort;
        $img = $model->img;

        if ($model->load(Yii::$app->request->post())) {

            Platform::updateSortEdit($model->sort, $sort);

            $model->short_description = mb_strimwidth($model->description, 0, 255, "...");

            $image = new ImageUpload;
            $platform = $this->findModel($model->id);
            $file = UploadedFile::getInstance($model, 'img');

            if($file)
            {
                $model->save();
                $image_save = $image->uploadFile($file, $model->img, $this->getFolder());

                $dir = $this->getFolder();
                $name = str_replace($dir, '', $image_save);
                $name = str_replace('/', '', $name);
                $platform->saveImage($name);

                //crop img
                /*$photo = Image::getImagine()->open($dir . $name);
                $photo->thumbnail(new Box(500, 325))->save($dir . 'thumbs/500x325_'. $name, ['quality' => 90]);
                $photo->thumbnail(new Box(800, 500))->save($dir . 'thumbs/800x500_'. $name, ['quality' => 90]);*/
            }
            else if($img && !$model->img)
            {
                $model->img = $img;
                $model->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $sort = $this->getListSort();
        return $this->render('update', [
            'model' => $model,
            'sort' => $sort,
            'status' => $this->getListStatus()
        ]);
    }

    public function getListSort()
    {
       $platform =  Platform::find()
           ->select('sort')
           ->orderBy('sort')
           ->asArray()
           ->all();

       return  ArrayHelper::map($platform, 'sort', 'sort');
    }

    public function getListSortNew()
    {
        $sort = $this->getListSort();
        if($sort){
            $last = array_pop($sort);
            array_push($sort, $last, (string)($last+1));
        }else{
            $sort[0] = (string)1;
        }

        return $sort;
    }

    public function getFolder()
    {
        return Yii::getAlias('@web') . 'uploads/platform/';
    }

    public function getListStatus()
    {
        return [Platform::STATUS_NOT_ACTIVE => 'No active', Platform::STATUS_ACTIVE => 'Active'];
    }

    /**
     * Deletes an existing Platform model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleteImage();

        Platform::updateSort($model->sort, true);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Platform model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Platform the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Platform::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
