<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Show;
use app\models\Event;
use yii\web\Controller;
use app\models\Platform;
use app\models\EventSerch;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSerch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();

        if ($model->load(Yii::$app->request->post())) {

            $model->date = date("Y-m-d H:i:s", strtotime($model->date));

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $events = Event::find()->select('show_id')->asArray()->all();
        $show_id = ArrayHelper::map($events, 'show_id', 'show_id');
        $show = Show::find()->where(['not in', 'id', $show_id])->all();

        return $this->render('create', [
            'model' => $model,
            'platforms' => ArrayHelper::map(Platform::find()->all(), 'id', 'title'),
            'shows' => ArrayHelper::map($show, 'id', 'title')
        ]);
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->date = date("Y-m-d H:i:s", strtotime($model->date));
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        //$events = Event::find()->select('show_id')->asArray()->all();
        $events = Event::find()->select('show_id')->where('id != :id', [':id' => $id])->asArray()->all();
        $show_id = ArrayHelper::map($events, 'show_id', 'show_id');

        $show = Show::find()->where(['not in', 'id', $show_id])->all();

        return $this->render('update', [
            'model' => $model,
            'platforms' => ArrayHelper::map(Platform::find()->all(), 'id', 'title'),
            'shows' => ArrayHelper::map($show, 'id', 'title')
        ]);
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
