<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Platform */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Platforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platform-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            ['format' => 'html',
                'label' => 'Description',
                'value' => function ($data) {
                    return $data->description;
                }
            ],
            ['format' => 'html',
                'label' => 'Img',
                'value' => function ($data) {
                    return Html::img($data->getImage(), ['width' => 200]);
                }],
            'sort',
            'created_at',
            'modified_at',
        ],
    ]) ?>

</div>
