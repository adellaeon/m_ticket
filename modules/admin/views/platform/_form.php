<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Platform */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="platform-form">

    <?php $form = ActiveForm::begin(['id' => 'platform-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), []); ?>

    <img class="preview-image" src="<?= $model->getImage(); ?>" style="width: 250px">
    <?= $form->field($model, 'img')->fileInput(['class' => 'form-control-file', 'onchange' => 'readURL(this);']) ?>

    <?= $form->field($model, 'sort')->dropDownList($sort, ['class' => 'form-control']); ?>
    <?= $form->field($model, 'status')->dropDownList($status, ['class' => 'form-control']); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.preview-image').attr('src', e.target.result).show();
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>