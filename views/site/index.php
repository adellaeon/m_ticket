<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */

$this->title = 'Список ближайших событий';
?>
<div class="index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <?php foreach ($events as $event): ?>
            <div class="col-md-4">
                <div class="card h-100">
                    <img class="img-responsive" src="<?= $event->show->getImage(); ?>">
                    <div class="card-body">
                        <h4>
                            <a href="<?= Url::toRoute(['/site/show', 'id' => $event->id]) ?>"><?= $event->show->title; ?></a>
                        </h4>
                        <p>Дата события: <?= $event->date; ?></p>
                        <p>Площадка: <a
                                    href="<?= Url::toRoute(['/site/event', 'id' => $event->platform->id]) ?>"><?= $event->platform->title; ?></a>
                        </p>
                        <p><?= $event->show->short_description; ?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
    ]);
    ?>
</div>