<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventSerch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = ['label' => $platform->title, 'url' => ['/site/platform']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="platform-view">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <img class="img-responsive" src="<?= $platform->getImage(); ?>">
            </div>
        </div>
        <div class="col-md-4">
            <div class="">
                <h4><a href="#"><?= $platform->title; ?></a></h4>
                <p><?= $platform->description; ?></p>
            </div>
        </div>
    </div>
</div>

<div class="event-view">
    <h1>События</h1>
    <?php if ($events): ?>
        <div class="row">
            <?php foreach ($events as $event): ?>
                <div class="col-md-4">
                    <div class="card h-100">
                        <img class="img-responsive" src="<?= $event->show->getImage(); ?>">
                        <div class="card-body">
                            <h4>
                                <a href="<?= Url::toRoute(['/site/show', 'id' => $event->id]) ?>"><?= $event->show->title; ?></a>
                            </h4>
                            <p>Дата события: <?= $event->date; ?></p>
                            <p>Площадка: <a
                                        href="<?= Url::toRoute(['/site/event', 'id' => $event->platform->id]) ?>"><?= $event->platform->title; ?></a>
                            </p>
                            <p><?= $event->show->short_description; ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php
        echo LinkPager::widget([
            'pagination' => $pagination,
        ]);
        ?>
    <?php else: ?>
        <div class="row">
            <div class="col-md-12"><p>События не найдены</p></div>
        </div>
    <?php endif; ?>
</div>
