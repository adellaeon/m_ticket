<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlatformSerch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Площадки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platform-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <?php foreach ($platforms as $platform): ?>
            <div class="col-md-3">
                <div class="card h-100">
                    <img class="img-responsive" src="<?= $platform->getImage(); ?>">
                    <div class="card-body">
                        <h4><?= $platform->title; ?></h4>
                        <p><?= $platform->short_description; ?></p>
                    </div>
                    <div class="card-footer">
                        <a href="<?= Url::toRoute(['/site/event', 'id' => $platform->id]) ?>" class="btn btn-primary">События
                            площадки</a>
                    </div>

                </div>

            </div>
        <?php endforeach; ?>
    </div>
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
    ]);
    ?>
</div>
