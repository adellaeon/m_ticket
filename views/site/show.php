<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShowSerch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shows';

$this->params['breadcrumbs'][] = ['label' => $event->platform->title, 'url' => ['/site/event?id=' . $event->platform->id]];
$this->params['breadcrumbs'][] = ['label' => $event->title, 'url' => ['/site/event?id=' . $event->platform->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if ($event): ?>
    <div class="event-view">
        <h1><?= $event->show->title ?></h1>
        <div class="row">
            <div class="col-md-12">
                <div class="">
                    <img class="img-responsive" src="<?= $event->show->getImage(); ?>">
                    <div class="card-body">
                        <h4><a href="#"><?= $event->show->title; ?></a></h4>
                        <p>Дата события: <?= $event->date; ?></p>
                        <p>Площадка: <a
                                    href="<?= Url::toRoute(['/site/event', 'id' => $event->platform->id]) ?>"><?= $event->platform->title; ?></a>
                        </p>
                        <p><?= $event->show->description; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>