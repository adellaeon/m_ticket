-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: mticket
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform_id` int(11) DEFAULT NULL,
  `show_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  KEY `idx-event-platform_id` (`platform_id`),
  KEY `idx-event-show_id` (`show_id`),
  CONSTRAINT `fk-event-platform_id` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-event-show_id` FOREIGN KEY (`show_id`) REFERENCES `show` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,1,1,'Title event','2018-05-03 14:50:00','2018-04-26 17:11:22','2018-04-26 17:11:22'),(3,1,2,'Title event1','2018-05-05 18:50:00','2018-04-28 14:17:00','2018-04-28 14:17:00');
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
INSERT INTO `migration` VALUES ('m000000_000000_base',1524669989),('m180424_215703_create_platform_table',1524751810),('m180424_215728_create_event_table',1524751810),('m180424_215738_create_show_table',1524751810),('m180425_154838_index_key_tables',1524751814),('m180425_162939_create_user_table',1524931973);
UNLOCK TABLES;

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
CREATE TABLE `platform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `description` text,
  `img` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  KEY `idx-platform-title` (`title`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platform`
--

LOCK TABLES `platform` WRITE;
/*!40000 ALTER TABLE `platform` DISABLE KEYS */;
INSERT INTO `platform` VALUES (1,'Platform 1','<p>Mauris dictum maximus interdum. Quisque feugiat lacus eget libero faucibus, eget aliquet leo mattis. Phasellus sed suscipit dui. Nulla facilisis mauris vel ante viverra laoreet. Praesent tempor, elit eget egestas ullamcorper, sem ipsum porta justo, ...','<p>Mauris dictum maximus interdum. Quisque feugiat lacus eget libero faucibus, eget aliquet leo mattis. Phasellus sed suscipit dui. Nulla facilisis mauris vel ante viverra laoreet. Praesent tempor, elit eget egestas ullamcorper, sem ipsum porta justo, pharetra elementum sem elit et ex. Quisque placerat condimentum cursus. Vivamus at nunc dolor. Donec pulvinar, eros eget volutpat mollis, lectus quam finibus tellus, vitae malesuada nibh velit eu orci. Donec magna libero, ullamcorper quis velit quis, pretium ultricies felis. Vestibulum posuere, tortor et ullamcorper aliquam, lacus erat accumsan felis, id vulputate quam est a nibh. Suspendisse id urna vestibulum, placerat elit sit amet, scelerisque sapien. Quisque commodo, ligula ut porta feugiat, neque metus consectetur libero, eu ornare arcu augue id nibh. Nam mattis dignissim fringilla. Mauris non venenatis elit, bibendum porta lacus. Praesent efficitur sapien vel erat condimentum, ac rhoncus ex vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer lobortis cursus est ut tincidunt. Morbi sollicitudin nec ipsum ut gravida. Proin egestas erat leo, nec venenatis nulla bibendum et. Maecenas sit amet efficitur elit, et sollicitudin ex. In consequat sapien vel rhoncus elementum. Nulla arcu ipsum, laoreet hendrerit eros eu, vehicula rutrum elit. In hac habitasse platea dictumst. Nulla nec massa quis justo cursus dictum in ac felis.</p>\r\n','/uploads/platform/30a666a44ba9ada89539a1c3204a52ac.jpg',1,'2018-04-26 17:10:38','2018-04-28 20:12:11'),(2,'Platform 2','Nulla dignissim felis est, blandit consequat neque fringilla id. Vivamus fringilla, sem eget dictum auctor, elit lectus sollicitudin tellus, eu sollicitudin nibh ex at mi. Sed a facilisis tellus. Phasellus sagittis dui justo, vel eleifend sem suscipit ...','Nulla dignissim felis est, blandit consequat neque fringilla id. Vivamus fringilla, sem eget dictum auctor, elit lectus sollicitudin tellus, eu sollicitudin nibh ex at mi. Sed a facilisis tellus. Phasellus sagittis dui justo, vel eleifend sem suscipit quis. Sed ullamcorper tempus vulputate. Quisque mauris nisi, facilisis non pharetra mollis, maximus vitae mauris. In hac habitasse platea dictumst. Proin at enim eget erat porta imperdiet. Cras eu hendrerit orci. Nullam eleifend vitae odio eget vestibulum. Morbi eu est consectetur, scelerisque urna nec, hendrerit odio. Suspendisse vel vulputate metus. Praesent vel sagittis velit. Morbi sagittis vestibulum nibh a vulputate. Ut tincidunt posuere dui, ut laoreet velit finibus non.','/uploads/platform/ecd6926d5daf89a37ff53be67be15826.jpg',6,'2018-04-26 23:57:31','2018-04-28 13:51:02'),(5,'Platform 3','Nam ullamcorper ultricies egestas. Etiam nisl metus, laoreet id suscipit nec, cursus ac risus. Fusce sed risus sit amet purus ultricies suscipit at vitae ex. Donec convallis eu nisl sit amet lacinia. Suspendisse pretium, nunc vel rutrum accumsan, neque...','Nam ullamcorper ultricies egestas. Etiam nisl metus, laoreet id suscipit nec, cursus ac risus. Fusce sed risus sit amet purus ultricies suscipit at vitae ex. Donec convallis eu nisl sit amet lacinia. Suspendisse pretium, nunc vel rutrum accumsan, neque ex euismod risus, a sodales tellus diam non lorem. Proin sed lacinia justo. Duis elementum, sem vel sollicitudin efficitur, turpis leo tempor ante, eu interdum leo felis quis elit. Donec a convallis nibh, et ullamcorper odio. Sed nulla quam, laoreet in sapien ut, gravida ullamcorper lorem. Morbi tristique bibendum eros, quis mollis ligula aliquam sed. Ut fermentum a orci ut porttitor. Pellentesque nisi lacus, interdum at nisl sed, dignissim molestie libero. In convallis cursus aliquet. Fusce mi nulla, tempor ut sapien non, feugiat aliquam nibh. Quisque eleifend ligula et felis accumsan maximus.','/uploads/platform/6e606cc3bfeb7d081ae7b3b4c0515959.jpg',4,'2018-04-27 21:18:32','2018-04-28 13:51:14'),(6,'Platform 4','Donec auctor tincidunt velit at tempus. Quisque facilisis blandit velit, ac lobortis purus volutpat et. Maecenas mattis quam a enim ultricies, id consectetur magna accumsan. Praesent lacinia aliquam mi, maximus volutpat felis ultrices quis. Aliquam por...','Donec auctor tincidunt velit at tempus. Quisque facilisis blandit velit, ac lobortis purus volutpat et. Maecenas mattis quam a enim ultricies, id consectetur magna accumsan. Praesent lacinia aliquam mi, maximus volutpat felis ultrices quis. Aliquam porttitor augue id ultrices consequat. Nullam sed est nulla. Nulla vulputate hendrerit diam vitae mollis. In iaculis non nunc vel dignissim. Maecenas nec mi in libero scelerisque consequat. Donec suscipit, orci quis consectetur pharetra, urna diam rhoncus urna, in vehicula leo mi et mi. Nunc bibendum tortor eu felis blandit sodales. Donec eleifend, sem quis convallis tincidunt, lorem sapien varius magna, quis viverra felis ligula ac felis. Nunc sed nulla mollis, sagittis justo eget, varius purus. Cras blandit urna sit amet condimentum lacinia. Vestibulum rutrum diam eget nulla mollis aliquam.','/uploads/platform/e59462609d647fda6972ab6d5b2d61a5.jpg',6,'2018-04-27 21:18:52','2018-04-28 13:51:26'),(7,'Platform 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sodales est ut rhoncus congue. Etiam turpis sapien, fermentum vel maximus eget, fermentum vitae nunc. Duis cursus tincidunt turpis vitae feugiat. Fusce elementum, ex eu finibus variu...','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sodales est ut rhoncus congue. Etiam turpis sapien, fermentum vel maximus eget, fermentum vitae nunc. Duis cursus tincidunt turpis vitae feugiat. Fusce elementum, ex eu finibus varius, mi tellus semper nulla, in molestie arcu elit et quam. Nunc euismod tortor neque, et vehicula enim porttitor venenatis. Etiam maximus sem a tellus imperdiet blandit. Ut tempus hendrerit lacus, non pharetra orci finibus consectetur. Nullam velit nulla, luctus id sollicitudin eu, rhoncus sed felis. Nulla nec velit felis. Phasellus hendrerit elit ut rhoncus sollicitudin. Quisque eget varius massa, id lacinia nisi.\r\n\r\nMorbi ligula diam, laoreet non maximus a, vestibulum quis sapien. Vivamus quis eleifend sem, porta pellentesque massa. Sed vestibulum leo quis commodo varius. Nunc non molestie justo, posuere sollicitudin nibh. Aliquam maximus eu ipsum venenatis scelerisque. Donec dapibus mauris eu magna tempor pulvinar. Morbi interdum vulputate vehicula. Donec non turpis suscipit, mattis lorem non, ornare augue. Proin vitae felis ac ex faucibus varius at non nibh. Integer purus nulla, consequat quis tristique sed, gravida eget tellus. Nulla ut lobortis urna, nec tristique nunc. Phasellus blandit sit amet mi non iaculis. Donec venenatis vestibulum est, a iaculis lacus malesuada id.\r\n\r\n','/uploads/platform/38a275ab46c2fab20db7c3ae565bc552.jpg',2,'2018-04-28 13:31:06','2018-04-28 13:51:41'),(8,'Platform 6','Nam nec leo lectus. Aliquam commodo neque quis sodales rhoncus. Praesent scelerisque leo nec ante mollis maximus. Phasellus faucibus ligula vel diam tincidunt, quis egestas ante ultrices. Duis purus orci, vulputate sed imperdiet eget, pharetra quis ris...','Nam nec leo lectus. Aliquam commodo neque quis sodales rhoncus. Praesent scelerisque leo nec ante mollis maximus. Phasellus faucibus ligula vel diam tincidunt, quis egestas ante ultrices. Duis purus orci, vulputate sed imperdiet eget, pharetra quis risus. Mauris ullamcorper varius tortor pulvinar dictum. Integer interdum tellus tincidunt, tempor turpis nec, cursus nunc. Praesent fringilla ante nibh, vitae euismod ex vulputate sed. Etiam facilisis feugiat lacinia.\r\n\r\nPellentesque suscipit finibus urna, in auctor lacus ornare a. Vestibulum lobortis condimentum cursus. Etiam enim erat, placerat et diam at, elementum auctor ipsum. Integer egestas scelerisque vulputate. Ut vehicula purus sapien, sit amet ultrices purus fermentum ut. In facilisis vulputate tortor id hendrerit. Duis erat ante, vehicula non ante quis, finibus mattis purus. Aliquam aliquam suscipit lorem. Sed velit nisl, elementum eu interdum eget, tincidunt vitae diam. Aliquam aliquet ultricies ipsum vestibulum placerat. Mauris sit amet arcu eget eros bibendum fringilla. Vivamus tempor sodales felis sed tempor. Suspendisse ac enim vel massa lacinia mattis id at massa. Ut nibh nibh, sollicitudin eu tincidunt nec, imperdiet quis mi. Vivamus rhoncus elementum neque ac maximus. Vivamus quam lorem, bibendum eu justo a, lobortis rutrum tellus.','/uploads/platform/624f53a3b4f4629ecef40094dd783fa2.jpg',5,'2018-04-28 13:31:40','2018-04-28 13:51:46'),(9,'nest1','grrr','grrr','/uploads/platform/a45099172077f7982aade71dcbe510a9.jpg',9,'2018-04-28 17:51:29','2018-04-28 17:51:29'),(10,'tmp5','fdrtgh','fdrtgh','/uploads/platform/e911046c4b52ffc15cec0d1b5f040692.jpg',11,'2018-04-28 17:58:21','2018-04-28 17:58:22'),(11,'rrr','zdxf','zdxf','/uploads/platform/42ec67332fa4ccb80aac25608ada34cf.jpg',13,'2018-04-28 17:59:41','2018-04-28 17:59:41'),(12,'dded','','','/uploads/platform/813cc98005fdf92d81a7025ac15dfa36.jpg',15,'2018-04-28 18:03:29','2018-04-28 18:03:29'),(14,'ghjhj','fggf','fggf','/uploads/platform/1f72d349bad7b1c12514a2b311a44931.jpg',18,'2018-04-28 18:08:08','2018-04-28 18:08:08'),(16,'testtt4','<p>Update Platform: te sttt4&nbsp;Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4</p>\r\n','<p>Update Platform: te sttt4&nbsp;Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4Update Platform: testtt4</p>\r\n','/uploads/platform/fa45f037d005d940e579e5399914f558.jpg',19,'2018-04-28 19:48:48','2018-04-28 19:51:47'),(17,'sdfghj','<p>dgg</p>\r\n','<p>dgg</p>\r\n','/uploads/platform/618787df3a8e7e7636c8eb2434f0fe4e.jpg',20,'2018-04-28 23:36:30','2018-04-28 23:36:31'),(18,'tterr','<p>fvgdx</p>\r\n','<p>fvgdx</p>\r\n','/uploads/platform/8ddbaacae377836ce48911d80dbcaf93.png',22,'2018-04-28 23:48:43','2018-04-28 23:48:43'),(23,'hkkh','<p>h</p>\r\n','<p>h</p>\r\n','',24,'2018-04-28 23:55:44','2018-04-28 23:55:44'),(24,'hjhk','<p>nm</p>\r\n','<p>nm</p>\r\n','',26,'2018-04-28 23:56:54','2018-04-28 23:56:54'),(25,'hjhktt','<p>nm</p>\r\n','<p>nm</p>\r\n','/uploads/platform/600e7d25e54d797d505ed268beb19e7a.jpg',27,'2018-04-28 23:58:08','2018-04-28 23:58:08'),(26,'cvc','<p>cv</p>\r\n','<p>cv</p>\r\n','',29,'2018-04-28 23:59:08','2018-04-28 23:59:08'),(27,'cvcyy','<p>cv</p>\r\n','<p>cv</p>\r\n','',31,'2018-04-28 23:59:44','2018-04-28 23:59:44'),(28,'cvcyyt','<p>cv</p>\r\n','<p>cv</p>\r\n','',33,'2018-04-29 00:03:16','2018-04-29 00:03:16'),(29,'cvcyythg','<p>cv</p>\r\n','<p>cv</p>\r\n','',35,'2018-04-29 00:03:42','2018-04-29 00:03:42'),(30,'cvcyythgh','<p>cv</p>\r\n','<p>cv</p>\r\n','',37,'2018-04-29 00:04:26','2018-04-29 00:04:26'),(31,'cvcyythghh','<p>cv</p>\r\n','<p>cv</p>\r\n','',39,'2018-04-29 00:05:04','2018-04-29 00:05:04'),(32,'cvcyythghhj','<p>cv</p>\r\n','<p>cv</p>\r\n','',41,'2018-04-29 00:05:19','2018-04-29 00:05:19'),(33,'Teest','<p>cv</p>\r\n','<p>cv</p>\r\n','/uploads/platform/1888e2253114b2a58683354ac39ba27c.jpg',42,'2018-04-29 00:07:13','2018-04-29 00:07:13');
/*!40000 ALTER TABLE `platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `show`
--

DROP TABLE IF EXISTS `show`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `show` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `description` text,
  `img` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  KEY `idx-show-title` (`title`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `show`
--

LOCK TABLES `show` WRITE;
/*!40000 ALTER TABLE `show` DISABLE KEYS */;
INSERT INTO `show` VALUES (1,'Show 1','Vivamus porttitor volutpat varius. Maecenas ut turpis nibh. Sed in semper sapien, in tempus nisi. Morbi rutrum hendrerit ex, ac accumsan libero maximus et. Cras odio elit, convallis eget sapien a, placerat consequat justo. In varius, dolor vel gravida ...','Vivamus porttitor volutpat varius. Maecenas ut turpis nibh. Sed in semper sapien, in tempus nisi. Morbi rutrum hendrerit ex, ac accumsan libero maximus et. Cras odio elit, convallis eget sapien a, placerat consequat justo. In varius, dolor vel gravida tincidunt, lorem risus sodales lorem, sit amet finibus lacus nunc sed odio. Nullam lacinia, mauris sit amet congue scelerisque, ex lectus elementum risus, quis fringilla dui nibh id lectus. Duis dictum varius felis. Nunc quis nulla sit amet mauris accumsan elementum ac non tellus. Aliquam placerat rhoncus neque id consectetur. Quisque interdum venenatis sapien.\r\n\r\nFusce non risus dictum, facilisis felis sed, finibus elit. Etiam lacus lectus, cursus et nunc eget, molestie vehicula mauris. Ut vehicula nisi nec velit imperdiet hendrerit eu eu arcu. Aliquam erat volutpat. In ac accumsan felis. Suspendisse interdum dolor eget massa auctor eleifend sit amet sit amet nunc. Cras pharetra dolor a mi tincidunt, eu placerat arcu interdum. Proin metus purus, luctus vel justo vestibulum, condimentum porta nisi. Pellentesque iaculis massa id leo tempor ultricies. Fusce scelerisque urna porta nisl viverra, eu volutpat metus fringilla. Etiam efficitur, quam et consectetur cursus, diam dui tempus magna, varius ullamcorper lacus orci in lorem. Etiam quis varius felis. Mauris viverra rutrum diam in fringilla. Integer finibus eros sit amet ex placerat, vel rutrum velit hendrerit. Nulla maximus maximus cursus. Fusce mauris odio, placerat a ultricies non, scelerisque ac risus.','/uploads/show/def1b08bb2b4f57863219286c09c06b3.jpg','2018-04-26 17:11:01','2018-04-28 14:15:59'),(2,'Show 2','Vivamus porttitor volutpat varius. Maecenas ut turpis nibh. Sed in semper sapien, in tempus nisi. Morbi rutrum hendrerit ex, ac accumsan libero maximus et. Cras odio elit, convallis eget sapien a, placerat consequat justo. In varius, dolor vel gravida ...','Vivamus porttitor volutpat varius. Maecenas ut turpis nibh. Sed in semper sapien, in tempus nisi. Morbi rutrum hendrerit ex, ac accumsan libero maximus et. Cras odio elit, convallis eget sapien a, placerat consequat justo. In varius, dolor vel gravida tincidunt, lorem risus sodales lorem, sit amet finibus lacus nunc sed odio. Nullam lacinia, mauris sit amet congue scelerisque, ex lectus elementum risus, quis fringilla dui nibh id lectus. Duis dictum varius felis. Nunc quis nulla sit amet mauris accumsan elementum ac non tellus. Aliquam placerat rhoncus neque id consectetur. Quisque interdum venenatis sapien.\r\n\r\nFusce non risus dictum, facilisis felis sed, finibus elit. Etiam lacus lectus, cursus et nunc eget, molestie vehicula mauris. Ut vehicula nisi nec velit imperdiet hendrerit eu eu arcu. Aliquam erat volutpat. In ac accumsan felis. Suspendisse interdum dolor eget massa auctor eleifend sit amet sit amet nunc. Cras pharetra dolor a mi tincidunt, eu placerat arcu interdum. Proin metus purus, luctus vel justo vestibulum, condimentum porta nisi. Pellentesque iaculis massa id leo tempor ultricies. Fusce scelerisque urna porta nisl viverra, eu volutpat metus fringilla. Etiam efficitur, quam et consectetur cursus, diam dui tempus magna, varius ullamcorper lacus orci in lorem. Etiam quis varius felis. Mauris viverra rutrum diam in fringilla. Integer finibus eros sit amet ex placerat, vel rutrum velit hendrerit. Nulla maximus maximus cursus. Fusce mauris odio, placerat a ultricies non, scelerisque ac risus.','/uploads/show/00792f7602071d2647eaaa0ad00ae37d.jpg','2018-04-28 14:16:09','2018-04-28 14:16:18'),(3,'retg','<p>dg</p>\r\n','<p>dg</p>\r\n','/uploads/show/20015b16861f81a36020980ab89a94e4.jpg','2018-04-28 20:12:27','2018-04-28 20:12:27');
/*!40000 ALTER TABLE `show` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `isAdmin` int(11) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','SOcP-iBcxuBeKoY9XC8qf1GsLP0D7Yxr','$2y$13$DuEi3Mni1hQbYbrD0ONJeOpCcW4gkUhNu0QMizotBQrvYbL5EWl/C',NULL,'admin@test.com',10,1,1524932033,1524932033),(2,'test','HTtvh1EPTw2bnUAosY_zvW4-TK1OD-Ey','$2y$13$GO2d15SkxRQKquuRLmoFrOeYY382S3A8rWnpkrhw/RjQPbG1kujcS',NULL,'test@gmail.com',10,0,1524932955,1524932955);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-29  0:11:54
