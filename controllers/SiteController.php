<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use app\models\Event;
use yii\web\Controller;
use app\models\Platform;
use yii\data\Pagination;
use app\models\LoginForm;
use app\models\SignupForm;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     * list of nearest events
     * @return string
     */
    public function actionIndex()
    {
        $query = Event::find()
            ->leftJoin('platform', 'platform.id= platform_id')
            ->where('date > :date AND  platform.status = :status',
                [
                    ':date' => date("Y-m-d H:i:s"),
                    ':status' => Platform::STATUS_ACTIVE
                ])
            ->orderBy('date');
        $countQuery = clone $query;
        $pagination = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 6]);
        $events = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'events' => $events,
            'pagination' => $pagination,
        ]);


        return $this->render('index');
    }

    /**
     * Displays platform list page.
     *
     * @return Response|string
     */

    public function actionPlatform()
    {
        $query = Platform::find()->where('status = :status', ['status' => Platform::STATUS_ACTIVE])->orderBy('sort');
        $countQuery = clone $query;
        $pagination = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 4]);
        $platforms = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('platform', [
            'platforms' => $platforms,
            'pagination' => $pagination,
        ]);

    }

    /**
     * Displays event list page.
     *
     * @return Response|string
     */

    public function actionEvent($id)
    {
        if (!$id) {
            return $this->redirect('/site/index');
        }

        $platform = Platform::findOne($id);

        if ($platform->status == Platform::STATUS_NOT_ACTIVE) {
            return $this->redirect('/site/index');
        }

        $query = Event::find()->where('platform_id = :platform_id', [':platform_id' => (int)$id]);
        $countQuery = clone $query;
        $pagination = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 4]);
        $events = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('event', [
            'platform' => $platform,
            'events' => $events,
            'pagination' => $pagination,
        ]);
    }

    /**
     * Displays show page.
     *
     * @return Response|string
     */

    public function actionShow($id)
    {
        if (!$id) {
            return $this->redirect('/site/index');
        }
        $event = Event::findOne($id);
        return $this->render('show', [
            'event' => $event,
        ]);

    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays signup page.
     *
     * @return Response|string
     */

    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /*    public function actionAddAdmin() {
            $model = User::find()->where(['username' => 'admin'])->one();
            if (empty($model)) {
                $user = new User();
                $user->username = 'admin';
                $user->email = 'admin@test.com';
                $user->setPassword('admin');
                $user->generateAuthKey();
                if ($user->save()) {
                    echo 'good';
                }
            }
        }*/
}
