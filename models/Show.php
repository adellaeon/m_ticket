<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "show".
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $img
 * @property string $created_at
 * @property string $modified_at
 *
 * @property Event[] $events
 */
class Show extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'show';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'modified_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['modified_at'],
                ],
                'value' => new Expression('NOW()'),
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description', 'short_description'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
            [['created_at', 'modified_at'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['modified_at'], 'default', 'value' => date('Y-m-d H:i:s')],
            [['title', 'short_description'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['img'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short_description' => 'Short description',
            'description' => 'Description',
            'img' => 'Img',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['show_id' => 'id']);
    }

    public function saveImage($filename)
    {
        $this->img = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->img) ? $this->img : '/img/no-image.png';
    }

    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->img);
    }
}
