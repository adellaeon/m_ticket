<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\ImageUpload;

/**
 * This is the model class for table "platform".
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $img
 * @property int $sort
 * @property int $status
 * @property string $created_at
 * @property string $modified_at
 *
 * @property Event[] $events
 */
class Platform extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE  = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'platform';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'modified_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['modified_at'],
                ],
                'value' => new Expression('NOW()'),
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description', 'short_description'], 'string'],
            [['sort', 'status'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['title', 'short_description'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['img'], 'file' ,'extensions' => 'png, jpg'],
            [['created_at', 'modified_at'], 'date', 'format'=>'php:Y-m-d H:i:s'],
            [['modified_at'], 'default', 'value'=>date('Y-m-d H:i:s')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short_description' => 'Short description',
            'description' => 'Description',
            'img' => 'Img',
            'sort' => 'Sort',
            'status' => 'Status',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['platform_id' => 'id']);
    }

    public function saveImage($filename)
    {
        $this->img = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->img) ? Yii::getAlias('@web') . '/uploads/platform/'. $this->img : '/img/no-image.png';
    }


    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->img);
    }


    static public function updateSort($sort, $minus = false)
    {
        $platforms = Platform::find()->where('sort >= :sort', [':sort' => (int)$sort])->all();
        if($platforms){
            foreach ($platforms as $platform){
                if($minus){
                    $platform->updateCounters(['sort'=> -1]);
                }else{
                    $platform->updateCounters(['sort'=>1]);
                }

            }
        }

        return true;

    }

    static public function updateSortEdit($sort, $sort_old = false)
    {
        if($sort_old < $sort){
            //ищем от $sort_old+1 до $sort и -1
            $arr = [];
            for($i = $sort_old+1; $i <= $sort; $i++ ){
                $arr[$i] = $i;
            }
            $platforms = Platform::find()->where(['in', 'sort', $arr])->all();
            if($platforms){
                foreach ($platforms as $platform){
                   $platform->updateCounters(['sort'=> -1]);
                }
            }

        }elseif($sort_old > $sort){
            //ищем от $sort до $sort_old - 1 и делаем +1
            $arr = [];
            for($i = $sort; $i <= $sort+1; $i++ ){
                $arr[$i] = $i;
            }
            $platforms = Platform::find()->where(['in', 'sort', $arr])->all();
            if($platforms){
                foreach ($platforms as $platform){
                    $platform->updateCounters(['sort'=> 1]);
                }
            }
        }
        return true;

    }
}
