<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUpload extends Model
{

    public $img;
    public $folder;

    public function rules()
    {
        return [
            [['img'], 'required'],
            [['img'], 'file', 'extensions' => 'jpg,png']
        ];
    }

    public function uploadFile(UploadedFile $file, $currentImage, $folder)
    {
        $this->img = $file;
        if ($this->validate()) {
            $this->folder = $folder;
            $this->deleteCurrentImage($currentImage);
            return $this->saveImage();
        }
    }

    private function generateFilename()
    {
        return strtolower(md5(uniqid($this->img->baseName)) . '.' . $this->img->extension);
    }

    public function deleteCurrentImage($currentImage)
    {
        if ($this->fileExists($currentImage)) {
            unlink($currentImage);
        }
    }

    public function fileExists($currentImage)
    {
        if (!empty($currentImage) && $currentImage != null) {
            return file_exists($currentImage);
        }
    }

    public function saveImage()
    {
        $filename = $this->generateFilename();
        $image = $this->folder . $filename;
        $this->img->saveAs($image);
        if (is_file(Yii::getAlias('@webroot') . '/' . $image)) {
            chmod(Yii::getAlias('@webroot') . '/' . $image, 0777);
        }
        return '/' . $image;
    }
}
