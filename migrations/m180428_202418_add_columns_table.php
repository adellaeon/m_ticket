<?php

use yii\db\Migration;

/**
 * Class m180428_202418_add_columns_table
 */
class m180428_202418_add_columns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('platform', 'status', $this->tinyInteger(1)->defaultValue(1)->after('sort'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('platform', 'status');
    }

}
