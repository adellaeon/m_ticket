<?php

use yii\db\Migration;

/**
 * Handles the creation of table `event`.
 */
class m180424_215728_create_event_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('event', [
            'id' => $this->primaryKey(),
            'platform_id' => $this->integer(),
            'show_id' => $this->integer(),
            'title' => $this->string(255)->notNull()->unique(),
            'date' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'modified_at' => $this->dateTime()
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('event');
    }
}
