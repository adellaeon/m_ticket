<?php

use yii\db\Migration;

/**
 * Class m180425_154838_index_key_tables
 */
class m180425_154838_index_key_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        // create index for column `title` for table `platform`
        $this->createIndex(
            'idx-platform-title',
            'platform',
            'title'
        );

        // create index for column `title` for table `show`
        $this->createIndex(
            'idx-show-title',
            'show',
            'title'
        );

        // create index for column `platform_id` for table `event`
        $this->createIndex(
            'idx-event-platform_id',
            'event',
            'platform_id'
        );

        // add foreign key for table `platform`
        $this->addForeignKey(
            'fk-event-platform_id',
            'event',
            'platform_id',
            'platform',
            'id',
            'CASCADE'
        );

        // create index for column `show_id` for table `event`
        $this->createIndex(
            'idx-event-show_id',
            'event',
            'show_id'
        );

        // add foreign key for table `event`
        $this->addForeignKey(
            'fk-event-show_id',
            'event',
            'show_id',
            'show',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops index for column `title` for table `platform`
        $this->dropIndex(
            'idx-platform-title',
            'platform'
        );

        // drops index for column `title` for table `show`
        $this->dropIndex(
            'idx-show-title',
            'show'
        );

        // drops foreign key for table `event`
        $this->dropForeignKey(
            'fk-event-platform_id',
            'event'
        );

        // drops index for column `platform_id` for table `event`
        $this->dropIndex(
            'idx-event-platform_id',
            'event'
        );


        // drops foreign key for table `event`
        $this->dropForeignKey(
            'fk-event-show_id',
            'event'
        );

        // drops index for column `show_id` for table `event`
        $this->dropIndex(
            'idx-event-show_id',
            'event'
        );

    }

}
