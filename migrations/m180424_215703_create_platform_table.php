<?php

use yii\db\Migration;

/**
 * Handles the creation of table `platform`.
 */
class m180424_215703_create_platform_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('platform', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull()->unique(),
            'short_description' => $this->string(255),
            'description' => $this->text(),
            'img' => $this->string(255),
            'sort' => $this->integer(),
            'created_at' => $this->dateTime(),
            'modified_at' => $this->dateTime()
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('platform');
    }
}
